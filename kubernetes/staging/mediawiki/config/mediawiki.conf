server {
    listen 8080;
    listen [::]:8080;

    server_name thumbnails-staging.pcgamingwiki.com;

    root /opt/mediawiki;

    client_max_body_size 16M;
    client_header_buffer_size 100k;
    large_client_header_buffers 4 100k;
    real_ip_header X-Forwarded-For;
    set_real_ip_from 10.0.0.0/8;

    location @allow {
        return 200;
    }

    location ~ ^/(archive/)?[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ {
		rewrite ^/[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ /w/thumb.php?f=$1&width=$2 break;
		rewrite ^/archive/[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ /w/thumb.php?f=$1&width=$2&archived=1 break;

        include fastcgi_params;
        fastcgi_param REMOTE_ADDR $http_x_real_ip;
        fastcgi_param SCRIPT_FILENAME	$document_root/w/thumb.php;
        fastcgi_pass 127.0.0.1:9000;
    }

    location /w/ {
        rewrite "^/w/img_auth.php/(.*)?" /w/img_auth.php?path=$1;
        # Do this inside of a location so it can be negated
        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_param REMOTE_ADDR $http_x_real_ip;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass 127.0.0.1:9000;
        }
    }

    location /w/cache       { deny all; }
    location /w/languages   { deny all; }
    location /w/maintenance { deny all; }
    location /w/serialized  { deny all; }
    # Just in case, hide .svn and .git too
    location ~ /\.(svn|git)(/|$) { deny all; }
    # Hide any .htaccess files
    location ~ /\.ht { deny all; }

    location / {
        return 404;
    }

}

server {
    listen 8080;
    listen [::]:8080;

    server_name staging.pcgamingwiki.com;

    root /opt/mediawiki;

    client_header_buffer_size 100k;
    large_client_header_buffers 4 100k;
    client_max_body_size 16M;
    real_ip_header X-Forwarded-For;
    set_real_ip_from 10.0.0.0/8;

    location @allow {
        return 200;
    }

    location /oauth2/ {
        proxy_buffer_size          128k;
        proxy_buffers              4 256k;
        proxy_busy_buffers_size    256k;
        proxy_pass       http://oauth2-proxy.sso-14681482-production.svc.cluster.local;
        proxy_set_header Host                    $host;
        proxy_set_header X-Real-IP               $remote_addr;
        proxy_set_header X-Scheme                $scheme;
        proxy_set_header X-Auth-Request-Redirect $scheme://$host$request_uri;
    }

    location = /oauth2/auth {
        proxy_pass       http://oauth2-proxy.sso-14681482-production.svc.cluster.local;
        proxy_set_header Host             $host;
        proxy_set_header X-Real-IP        $remote_addr;
        proxy_set_header X-Scheme         $scheme;
        # nginx auth_request includes headers but not body
        proxy_set_header Content-Length   "";
        proxy_pass_request_body           off;
        proxy_intercept_errors on;
        error_page 401 = @allow;
    }

	location /w/ {
	    rewrite "^/w/img_auth.php/(.*)?" /w/img_auth.php?path=$1;
		# Do this inside of a location so it can be negated
		location ~ \.php$ {
            auth_request /oauth2/auth;

            auth_request_set $oidc_user $upstream_http_x_auth_request_user;
            auth_request_set $oidc_email $upstream_http_x_auth_request_email;
            auth_request_set $oidc_username $upstream_http_x_auth_request_preferred_username;

            include fastcgi_params;
            fastcgi_param REMOTE_ADDR $http_x_real_ip;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_param HTTP_X_Forwarded_User $oidc_user;
            fastcgi_param HTTP_X_Forwarded_Email $oidc_email;
            fastcgi_param HTTP_X_Forwarded_Preferred_Username $oidc_username;
            fastcgi_pass 127.0.0.1:9000;
        }
    }

    location /w/images {}

    location ~ ^/w/images/thumb/(archive/)?[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ {
		rewrite ^/w/images/thumb/[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ /w/thumb.php?f=$1&width=$2;
		rewrite ^/w/images/thumb/archive/[0-9a-f]/[0-9a-f][0-9a-f]/([^/]+)/([0-9]+)px-.*$ /w/thumb.php?f=$1&width=$2&archived=1;

		include fastcgi_params;
		fastcgi_param REMOTE_ADDR $http_x_real_ip;
		fastcgi_param SCRIPT_FILENAME	$document_root/w/thumb.php;
		fastcgi_pass 127.0.0.1:9000;
    }

    location /w/images/deleted {deny all;}
    location /w/cache       { deny all; }
    location /w/languages   { deny all; }
    location /w/maintenance { deny all; }
    location /w/serialized  { deny all; }
	# Just in case, hide .svn and .git too
	location ~ /\.(svn|git)(/|$) { deny all; }
    # Hide any .htaccess files
    location ~ /\.ht { deny all; }

    location ~ ^/static/ {
        try_files $uri 404;
        add_header Cache-Control "public";
        expires 7d;
    }

    location ~ ^/w/(skins|extensions)/.+\.(css|js|gif|jpg|jpeg|png|svg)$ {
        try_files $uri 404;
        add_header Cache-Control "public";
        expires 7d;
    }

    location /wiki/ {
        rewrite ^/wiki/(?<pagename>.*)$ /w/index.php break;

        auth_request /oauth2/auth;

        auth_request_set $oidc_user $upstream_http_x_auth_request_user;
        auth_request_set $oidc_email $upstream_http_x_auth_request_email;
        auth_request_set $oidc_username $upstream_http_x_auth_request_preferred_username;

        include fastcgi_params;
        fastcgi_param REMOTE_ADDR $http_x_real_ip;
        fastcgi_param SCRIPT_FILENAME $document_root/w/index.php;
        fastcgi_param PATH_INFO $pagename;
        fastcgi_param QUERY_STRING $query_string;
        fastcgi_param HTTP_X_Forwarded_User $oidc_user;
        fastcgi_param HTTP_X_Forwarded_Email $oidc_email;
        fastcgi_param HTTP_X_Forwarded_Preferred_Username $oidc_username;
        fastcgi_pass 127.0.0.1:9000;
    }

    location /w/mw-config/ {
        location ~ \.php$ {
            include fastcgi_params;
            fastcgi_param REMOTE_ADDR $http_x_real_ip;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            fastcgi_pass 127.0.0.1:9000;
        }
    }

    location = / {
        return 301 $scheme://$http_host/wiki/Main_Page;
    }

    location /robots.txt {
    }

    location / {
        return 404;
    }
}
