<?php

# Protect against web entry
if (!defined('MEDIAWIKI')) {
    exit;
}

function config_getHosts(string $serviceName, bool $ipOnly = false)
{
    $hostsInCache = false;

    $hosts = apcu_fetch("$serviceName-hosts", $hostsInCache);

    if (!$hostsInCache) {
        $srv = dns_get_record($serviceName, DNS_SRV);

        $hosts = array_map(function ($record) {
            return $record["target"];
        }, $srv);

        if ($ipOnly) {
            $srvHosts = $hosts;

            $hosts = array_map(function ($target) {
                $r = dns_get_record($target, DNS_A);
                return $r[0]['ip'];
            }, $srvHosts);
        }

        apcu_add("$serviceName-hosts", $hosts, 60);
    }

    return $hosts;
}

function config_getSecrets()
{
    $secretsInCache = false;

    $secrets = apcu_fetch("mw-secrets", $secretsInCache);

    if (!$secretsInCache) {
        $secrets = [];

        $files = scandir('/opt/mediawiki-secrets');
        $secretFiles = array_filter($files, function ($fileName) {
            return !(substr($fileName, 0, 1) === '.');
        });

        foreach ($secretFiles as $fileName) {
            $secrets[$fileName] = file_get_contents("/opt/mediawiki-secrets/$fileName");
        }

        apcu_add("mw-secrets", $secrets, 600);
    }

    return $secrets;
}

function config_generateFileBackend(string $wiki, string $key, string $secret, string $region, bool $sharded, string $bucketBase = null)
{
    if ($bucketBase === null) {
        $bucketBase = $wiki;
    }
    $config = [
        'name' => 'AmazonS3',
        'class' => 'AmazonS3FileBackend',
        'awsKey' => $key,
        'awsSecret' => $secret,
        'awsRegion' => $region,
        'endpoint' => "https://$region.digitaloceanspaces.com",
        'lockManager' => 'nullLockManager',
        'containerPaths' => [
            "$wiki-local-deleted" => "$bucketBase-deleted",
            "$wiki-local-temp" => "$bucketBase-temp"
        ]
    ];
    if ($sharded) {
        $config['shardViaHashLevels'] = [
            'local-public' => ['levels' => 1, 'base' => 16, 'repeat' => true],
            'local-thumb' => ['levels' => 1, 'base' => 16, 'repeat' => true]
        ];

        foreach (range(0, 15) as &$i) {
            $suffix = base_convert($i, 10, 16);
            $config['containerPaths']["$wiki-local-public.$suffix"] = "$bucketBase-$suffix";
            $config['containerPaths']["$wiki-local-thumb.$suffix"] = "$bucketBase-thumbs-$suffix";
        }
    } else {
        $config['containerPaths']["$wiki-local-public"] = "$bucketBase";
        $config['containerPaths']["$wiki-local-thumb"] = "$bucketBase-thumbs";
    }

    return ['s3' => $config];
}

function config_generateLocalFileRepo(string $rootDomain, string $imagesDomain = null, string $thumbnailsDomain = null)
{
    if ($imagesDomain === null) {
        $imagesDomain = "https://images.$rootDomain";
    }

    if ($thumbnailsDomain === null) {
        $thumbnailsDomain = "https://thumbnails.$rootDomain";
    }

    $config = [
        'class' => 'LocalRepo',
        'name' => 'local',
        'backend' => 'AmazonS3',
        'url' => '/w/img_auth.php',
        'hashLevels' => 2,
        'deletedHashLevels' => 2,
        'transformVia404' => true,
        'zones' => [
            'public' => [
                'url' => $imagesDomain
            ],
            'thumb' => [
                'url' => $thumbnailsDomain
            ],
            'deleted' => [
                'url' => false
            ],
            'temp' => [
                'url' => false
            ]
        ]
    ];

    return $config;
}

function config_configureNamespaces(array $nsconfig)
{
    global $wgExtraNamespaces, $wgNamespacesWithSubpages, $wgNamespaceProtection;
    foreach ($nsconfig as $ns => $settings) {
        $id = $settings['id'];
        define($ns, $id);
        $wgExtraNamespaces[$id] = $settings['name'];
        $wgNamespacesWithSubpages[$id] = true;
        if (isset($settings['protection'])) {
            $permissionName = $settings['protection'];
            $wgNamespaceProtection[$id] = [$permissionName];
        }
    }
}


/*
 * We have to set a couple of default permissions up here because MediaWiki's SiteConfiguration object makes `true`
 * values take precedence when merging arrays.
 * See: https://doc.wikimedia.org/mediawiki-core/master/php/SiteConfiguration_8php_source.html#l00595
 */
$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['createpage'] = false;
$wgGroupPermissions['*']['skipcaptcha'] = false;
$wgGroupPermissions['user']['createpage'] = false;
$wgGroupPermissions['user']['skipcaptcha'] = false;

$pcgwSecrets = config_getSecrets();

/** @noinspection PhpUndefinedVariableInspection */
$wgConf->settings = [
    'wgSitename' => [
        'pcgamingwiki_staging' => 'PCGamingWiki - Staging'
    ],
    // System configuration
    'wgDBservers' => [
        'default' => array_map(function (string $host) {
            global $pcgwSecrets;
            /** @noinspection PhpUndefinedConstantInspection */
            return [
                'host' => $host,
                'user' => 'mediawiki',
                'password' => $pcgwSecrets["DB_PASSWORD"],
                'type' => 'mysql',
                'flags' => DBO_DEFAULT,
                'load' => 1,
                'useGTIDs' => true,
                'lagDetectionMethod' => 'Seconds_Behind_Master',
                'max lag' => '30'
            ];
        }, config_getHosts('mysql'))
    ],
    'wgDBTableOptions' => [
        'default' => 'ENGINE=InnoDB, DEFAULT CHARSET=binary'
    ],
    'wgLanguageCode' => [
        'default' => 'en'
    ],
    'wgShellLocale' => [
        'default' => 'en_US.utf8'
    ],
    // Page caching
    'wgUseCdn' => [
        'default' => true
    ],
    'wgCdnServers' => [
        'default' => array_map(function (string $host) {
            return "$host:8080";
        }, config_getHosts('trafficserver', true))
    ],
    'wgCdnServersNoPurge' => [
        'default' => ['10.0.0.0/8']
    ],
    'wgUsePrivateIPs' => [
        'default' => false
    ],
    'wgUseETag' => [
        'default' => true
    ],
    // Object caching
    '+wgObjectCaches' => [
        'default' => [
            'memcached-pecl' => [
                'servers' => array_map(function (string $host) {
                    return "$host:11211";
                }, config_getHosts('memcached'))
            ]
        ]
    ],
    'wgMainCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgParserCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgMessageCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgSessionCacheType' => [
        'default' => CACHE_DB
    ],
    'wgUseLocalMessageCache' => [
        'default' => true
    ],
    'wgEnableSidebarCache' => [
        'default' => true
    ],
    'wgParserCacheExpireTime' => [
        'default' => '86400'
    ],
    // Performance
    'wgJobRunRate' => [
        'default' => 0
    ],
    'wgDisableCounters' => [
        'default' => true
    ],
    'wgMiserMode' => [
        'default' => true
    ],
    'wgCategoryPagingLimit' => [
        'default' => 9600
    ],
    // URL settings
    'wgServer' => [
        'pcgamingwiki_staging' => 'https://staging.pcgamingwiki.com'
    ],
    'wgInternalServer' => [
        'pcgamingwiki_staging' => 'http://staging.pcgamingwiki.com'
    ],
    'wgScriptPath' => [
        'default' => '/w'
    ],
    'wgArticlePath' => [
        'default' => '/wiki/$1'
    ],
    'wgUsePathInfo' => [
        'default' => true
    ],
    // Email settings
    'wgEnableEmail' => [
        'default' => true
    ],
    'wgEnableUserEmail' => [
        'default' => true
    ],
    'wgEnotifUserTalk' => [
        'default' => false
    ],
    'wgEnotifWatchlist' => [
        'default' => false
    ],
    'wgEmailAuthentication' => [
        'default' => true
    ],
    'wgEmergencyContact' => [
        'default' => 'contact@pcgamingwiki.com'
    ],
    'wgPasswordSender' => [
        'default' => 'no-reply@pcgamingwiki.com'
    ],
    // File settings
    'wgEnableUploads' => [
        'default' => true
    ],
    'wgUseInstantCommons' => [
        'default' => false
    ],
    'wgUseImageMagick' => [
        'default' => true
    ],
    'wgImageMagickConvertCommand' => [
        'default' => '/usr/bin/convert'
    ],
    'wgAllowTitlesInSVG' => [
        'default' => true
    ],
    '+wgFileExtensions' => [
        'default' => ['svg']
    ],
    'wgSVGConverter' => [
        'default' => 'rsvg'
    ],
    'wgMaxUploadSize' => [
        'default' => 1024 * 1024 * 16
    ],
    'wgUploadSizeWarning' => [
        'default' => 1024 * 1024 * 16
    ],
    // File storage
    'wgFileBackends' => [
        'pcgamingwiki_staging' => config_generateFileBackend('pcgamingwiki_staging', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false, "pcgw-staging")
    ],
    'wgLocalFileRepo' => [
        'pcgamingwiki_staging' => config_generateLocalFileRepo('staging.pcgamingwiki.com',
            'https://images-staging.pcgamingwiki.com',
            'https://thumbnails-staging.pcgamingwiki.com')
    ],
    // Authentication & Authorization
    'wgAutoConfirmAge' => [
        'default' => 21600
    ],
    'wgAutoConfirmCount' => [
        'default' => 3
    ],
    'wgAutopromote' => [
        'default' => [
            "autoconfirmed" => ["&",
                [APCOND_EDITCOUNT, &$wgAutoConfirmCount],
                [APCOND_AGE, &$wgAutoConfirmAge],
            ],
            "emailconfirmed" => APCOND_EMAILCONFIRMED,
        ]
    ],
    '+wgNamespaceProtection' => [
        '+pcgamingwiki_staging' => [
            NS_TEMPLATE => 'edittemplate',
            NS_PROJECT => 'editproject',
            NS_PROJECT_TALK => 'editprojecttalk'
        ]
    ],
    '+wgGroupPermissions' => [
        'default' => [
            '*' => [
                'skipcaptcha' => false,
                'edit' => false,
                'createpage' => false
            ],
            'sysop' => [
                'createpage' => true,
                'usermerge' => true,
                'deletelogentry' => true,
                'deleterevision' => true,
                'interwiki' => true,
                'skipcaptcha' => true
            ],
            'user' => [
                'skipcaptcha' => false,
                'createpage' => false,
                'upload' => false,
                'editprojecttalk' => true
            ],
            'emailconfirmed' => [
                'skipcaptcha' => false
            ],
            'bot' => [
                'skipcaptcha' => true
            ],
            'Trusted' => [
                'skipcaptcha' => true,
                'suppressredirect' => true
            ]
        ],
        '+pcgamingwiki_staging' => [
            '*' => [
                'skipcaptcha' => false,
                'createpage' => false,
                'editstore' => false,
                'editcompany' => false,
                'editglossary' => false,
                'editcontroller' => false,
                'editemulation' => false,
                'editguide' => false,
                'editengine' => false,
                'editseries' => false
            ],
            'sysop' => [
                'usermerge' => true,
                'deletelogentry' => true,
                'deleterevision' => true,
                'interwiki' => true,
                'skipcaptcha' => true,
                'edittemplate' => true,
                'editdevelopment' => true,
                'editwidgets' => true,
                'editstore' => true,
                'editcompany' => true,
                'editglossary' => true,
                'editcontroller' => true,
                'editemulation' => true,
                'editguide' => true,
                'editengine' => true,
                'editseries' => true,
                'editproject' => true
            ],
            'user' => [
                'skipcaptcha' => false,
                'createpage' => false,
                'upload' => false,
                'edituser' => false
            ],
            'bureaucrat' => [
                'createpage' => false,
                'interwiki' => true,
                'usermerge' => true
            ]
        ]
    ],
    'wgPluggableAuth_ButtonLabel' => [
        'default' => 'Login with PCGamingWiki Account'
    ],
    'wgPluggableAuth_EnableLocalLogin' => [
        'pcgamingwiki_staging' => true
    ],
    'wgPluggableAuth_EnableLocalProperties' => [
        'default' => true
    ],
    'wgOpenIDConnect_Config' => [
        'default' => [
            'https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/' => [
                'name' => 'PCGamingWiki Account',
                'clientID' => 'mediawiki',
                'clientsecret' => $pcgwSecrets["OIDC_SECRET"],
                'scope' => ['openid', 'profile', 'email']
            ]
        ]
    ],
    'wgOpenIDConnect_MigrateUsersByEmail' => [
        'default' => true
    ],
    'wgOpenIDConnect_ForceLogout' => [
        'default' => true
    ],
    // UX
    'wgHiddenPrefs' => [
        'default' => []
    ],
    'wgAllowUserCss' => [
        'default' => true
    ],
    'wgAllowUserJs' => [
        'default' => true
    ],
    'wgResourceLoaderMaxQueryLength' => [
        'default' => -1
    ],
    '+wgDefaultUserOptions' => [
        'default' => [
            'usebetatoolbar' => 1,
            'usebetatoolbar-cgd' => 1,
            'wikieditor-preview' => 1
        ],
        '+pcgamingwiki_staging' => [
            'wikieditor-preview' => 0
        ]
    ],
    'wgLogo' => [
        'pcgamingwiki_staging' => 'https://static.pcgamingwiki.com/logos/pcgamingwiki.svg'
    ],
    'wgFavicon' => [
        'pcgamingwiki_staging' => 'https://static.pcgamingwiki.com/favicons/pcgamingwiki.png'
    ],
    'wgDefaultSkin' => [
        'pcgamingwiki_staging' => 'overclocked'
    ],
    // Licensing
    'wgRightsUrl' => [
        'default' => '//creativecommons.org/licenses/by-nc-sa/3.0'
    ],
    'wgRightsText' => [
        'default' => 'Creative Commons Attribution Non-Commercial Share Alike'
    ],
    '+wgFooterIcons' => [
        'default' => [
            '+poweredby' => [
                'myicon' => [
                    "src" => "https://static.pcgamingwiki.com/images/PoweredBy.png",
                    "url" => "https://pcgamingwiki.com/",
                    "alt" => "Brought to you by PCGamingWiki",
                    "height" => "31",
                    "width" => "100",
                ]
            ]
        ]
    ],
    // Flow
    'wgNamespaceContentModels' => [
        'default' => [
            NS_TALK => 'flow-board',
            NS_USER_TALK => 'flow-board',
            NS_PROJECT_TALK => 'flow-board',
            NS_FILE_TALK => 'flow-board',
            NS_MEDIAWIKI_TALK => 'flow-board',
            NS_TEMPLATE_TALK => 'flow-board',
            NS_HELP_TALK => 'flow-board',
            NS_CATEGORY_TALK => 'flow-board'
        ]
    ],
    'wgFlowContentFormat' => [
        'default' => 'wikitext'
    ],
    // Misc
    'wgLqtTalkPages' => [
        'default' => false
    ],
    // Captcha
    'wgCaptchaClass' => [
        'default' => 'ReCaptchaNoCaptcha'
    ],
    'wgReCaptchaSiteKey' => [
        'default' => $pcgwSecrets["RECAPTCHA_SITEKEY"]
    ],
    'wgReCaptchaSecretKey' => [
        'default' => $pcgwSecrets["RECAPTCHA_SECRETKEY"]
    ],
    'wgCaptchaTriggers' => [
        'default' => [
            'edit' => false,
            'create' => false,
            'addurl' => true,
            'createaccount' => true,
            'badlogin' => true
        ]
    ],
    // Extension loading
    'pcgwLoadExtensions' => [
        'default' => [
            'Auth_remoteuser',
            'AWS',
            'CheckUser',
            'Cite',
            'ConfirmEdit',
            'ConfirmEdit/ReCaptchaNoCaptcha',
            'Echo',
            'Flow',
            'HTMLets',
            'intersection',
            'Interwiki',
            'MultimediaViewer',
            'Nuke',
            'KeycloakAuth',
            'PageForms',
            'ParserFunctions',
            'Renameuser',
            'SpamBlacklist',
            'SubpageFun',
            'Thanks',
            'TitleBlacklist',
            'TitleKey',
            'UserMerge',
            'Variables',
            'WikiEditor',
        ],
        '+pcgamingwiki_staging' => [
            'Cargo',
            'ExternalData',
            'Widgets'
        ]
    ],
    'pcgwLoadSkins' => [
        'default' => [
            'Vector',
            'pcgwnvector'
        ],
        'pcgamingwiki_staging' => [
            'overclocked'
        ]
    ],
    // Namespace stuff
    'wgNamespacesWithSubpages' => [
        '+pcgamingwiki_staging' => array_fill(0, 500, true)
    ],
    'pcgwDefineNamespaces' => [
        'pcgamingwiki_staging' => [
            "NS_DEVELOPMENT" => ['id' => 400, 'name' => 'Development', 'protection' => 'editdevelopment'],
            "NS_DEVELOPMENT_TALK" => ['id' => 401, 'name' => 'Development_talk'],
            "NS_SERIES" => ['id' => 402, 'name' => 'Series', 'protection' => 'editseries'],
            "NS_SERIES_TALK" => ['id' => 403, 'name' => 'Series_talk'],
            "NS_ENGINE" => ['id' => 404, 'name' => 'Engine', 'protection' => 'editengine'],
            "NS_ENGINE_TALK" => ['id' => 405, 'name' => 'Engine_talk'],
            "NS_GUIDE" => ['id' => 406, 'name' => 'Guide', 'protection' => 'editguide'],
            "NS_GUIDE_TALK" => ['id' => 407, 'name' => 'Guide_talk'],
            "NS_EMULATION" => ['id' => 410, 'name' => 'Emulation', 'protection' => 'editemulation'],
            "NS_EMULATION_TALK" => ['id' => 411, 'name' => 'Emulation_talk'],
            "NS_CONTROLLER" => ['id' => 412, 'name' => 'Controller', 'protection' => 'editcontroller'],
            "NS_CONTROLLER_TALK" => ['id' => 413, 'name' => 'Controller_talk'],
            "NS_GLOSSARY" => ['id' => 414, 'name' => 'Glossary', 'protection' => 'editglossary'],
            "NS_GLOSSARY_TALK" => ['id' => 415, 'name' => 'Glossary_talk'],
            "NS_COMPANY" => ['id' => 416, 'name' => 'Company', 'protection' => 'editcompany'],
            "NS_COMPANY_TALK" => ['id' => 417, 'name' => 'Company_talk'],
            "NS_STORE" => ['id' => 418, 'name' => 'Store', 'protection' => 'editstore'],
            "NS_STORE_TALK" => ['id' => 419, 'name' => 'Store_talk']
        ]
    ],
    'pcgwDefaultSearchNamespaces' => [
        'pcgamingwiki_staging' => [
            'NS_MAIN' => true,
            'NS_TALK' => true,
            'NS_SERIES' => true,
            'NS_SERIES_TALK' => false,
            'NS_ENGINE' => true,
            'NS_ENGINE_TALK' => false,
            'NS_COMPANY' => true,
            'NS_COMPANY_TALK' => false,
            'NS_GLOSSARY' => true,
            'NS_GLOSSARY_TALK' => false,
            'NS_STORE' => true,
            'NS_STORE_TALK' => false,
            'NS_GUIDE' => true,
            'NS_GUIDE_TALK' => false,
            'NS_USER' => false,
            'NS_USER_TALK' => false,
            'NS_PROJECT' => false,
            'NS_PROJECT_TALK' => false,
            'NS_IMAGE' => false,
            'NS_IMAGE_TALK' => false,
            'NS_MEDIAWIKI' => false,
            'NS_MEDIAWIKI_TALK' => false,
            'NS_TEMPLATE' => false,
            'NS_TEMPLATE_TALK' => false,
            'NS_HELP' => false,
            'NS_HELP_TALK' => false,
            'NS_CATEGORY' => false,
            'NS_CATEGORY_TALK' => false
        ]
    ],
    'pcgwSemanticLinkNamespaces' => [
        'pcgaminngwiki' => [
            'NS_SERIES' => true,
            'NS_ENGINE' => true,
            'NS_COMPANY' => true,
            'NS_USER' => false,
            'NS_PROJECT' => false,
            'NS_GUIDE' => false,
            'NS_GLOSSARY => false'
        ]
    ],
    // Semantic MediaWiki
    '$smwgEnabledCompatibilityMode' => [
        'default' => true
    ],
    'smwgQMaxInlineLimit' => [
        'default' => 5000
    ],
    'smwgEnableUpdateJobs' => [
        'default' => true
    ],
    'smwgQueryResultCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'smwgCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'smwgEnabledEditPageHelp' => [
        'default' => false
    ],
    // Cargo
    'wgCargoDBname' => [
        'pcgamingwiki_staging' => 'pcgamingwiki_staging_cargo'
    ],
    'wgCargoDBuser' => [
        'default' => 'mediawiki_cargo'
    ],
    'wgCargoDBpassword' => [
        'default' => $pcgwSecrets["CARGO_DB_PASSWORD"]
    ],
    'wgCargoAllowedSQLFunctions' => [
        'default' => [
            'RAND'
        ]
    ],
    'wgCargoDBserver' => [
        'default' => config_getHosts('mysql')[0]
    ],
    // External Data
    'edgAllowExternalDataFrom' => [
        'default' => [
            "https://api.isthereanydeal.com/",
            "https://piwik.pcgamingwiki.com/",
            "http://m.co-optimus.com"
        ]
    ],
    'edgStringReplacements' => [
        'default' => [
            'ITAD_KEY' => $pcgwSecrets["ITAD_KEY"]
        ]
    ],
    'edgCacheTable' => [
        'default' => 'ed_url_cache'
    ],
    'edgCacheExpireTime' => [
        'default' => 1 * 24 * 60 * 60
    ],
    // Misc
    'wgDebugComments' => [
        'pcgamingwiki_staging' => true
    ],
    // Email
    'wgSMTP' => [
        'default' => [
            'host' => '10.132.212.225',
            'IDHost' => 'pcgamingwiki.com',
            'port' => 25,
            'auth' => false
        ]
    ],
    // Debugging
    'wgShowSQLErrors' => [
        'default' => true
    ],
    'wgShowDBErrorBacktrace' => [
        'default' => true
    ],
    // SSO
    'wgKeycloakAuthLoginUrl' => [
        'default' => 'https://auth.pcgamingwiki.com/oauth2/start?rd=$1'
    ],
    'wgKeycloakAuthLogoutUrl' => [
        'default' => 'https://auth.pcgamingwiki.com/oauth2/sign_out?rd=https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/protocol/openid-connect/logout?redirect_uri=$1'
    ],
    'wgKeycloakAuthPortalUrl' => [
        'default' => 'https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/account'
    ],
    'wgKeycloakAuthInsecureHeaders' => [
        'default' => true
    ]
];


if (defined('MW_DB')) {
    $wgDBname = MW_DB;
} else {
    $host = '';
    if (isset($_SERVER["HTTP_X_FORWARDED_HOST"])) {
        $host = $_SERVER["HTTP_X_FORWARDED_HOST"];
    } else {
        $host = $_SERVER["HTTP_HOST"];
    }
    switch ($host) {
        case 'staging.pcgamingwiki.com':
        case 'images-staging.pcgamingwiki.com':
        case 'thumbnails-staging.pcgamingwiki.com':
            $wgDBname = 'pcgamingwiki_staging';
            break;
        default:
            header('HTTP/1.1 404 Not Found');
            $host = $_SERVER['HTTP_HOST'];
            echo 'You shouldn\'t be seeing this. If this truly is an error, please notify an administrator.';
            echo '\n';
            echo "Couldn't map $host to a wiki";
            exit(0);
    }
}

$pcgwLoadExtensions = [];
$pcgwLoadSkins = [];
$wgConf->extractAllGlobals($wgDBname);

foreach ($pcgwLoadExtensions as &$extension) {
    switch ($extension) {
        case 'googleAnalytics':
            require_once "$IP/extensions/googleAnalytics/googleAnalytics.php";
            break;
        case 'HTMLets':
            require_once "$IP/extensions/HTMLets/HTMLets.php";
            break;
        case 'SubpageFun':
            require_once "$IP/extensions/SubpageFun/SubpageFun.php";
            break;
        default:
            wfLoadExtension($extension);
    }
}

foreach ($pcgwLoadSkins as &$skin) {
    switch ($skin) {
        case 'pcgwnvector':
            require_once "$IP/skins/pcgwnvector/pcgwnvector.php";
            break;
        case 'appleclocked':
            require_once "$IP/skins/appleclocked/Appleclocked.php";
            break;
        case 'overclocked':
            require_once "$IP/skins/overclocked/Overclocked.php";
            break;
        default:
            wfLoadSkin($skin);
    }
}

if (isset($pcgwDefineNamespaces)) {
    config_configureNamespaces($pcgwDefineNamespaces);
}

if ( !defined( 'STDERR' ) ) {
    define( 'STDERR', fopen( 'php://stderr', 'w' ) );
}

if ( !isset( $maintClass ) || ( isset( $maintClass ) && $maintClass !== 'PHPUnitMaintClass' ) ) {
    $wgMWLoggerDefaultSpi = [
        'class' => \MediaWiki\Logger\ConsoleSpi::class,
    ];
}

