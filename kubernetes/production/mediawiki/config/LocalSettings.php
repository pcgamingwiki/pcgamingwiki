<?php

# Protect against web entry
if (!defined('MEDIAWIKI')) {
    exit;
}

function config_getHosts(string $serviceName, bool $ipOnly = false)
{
    $hostsInCache = false;

    $hosts = apcu_fetch("$serviceName-hosts", $hostsInCache);

    if (!$hostsInCache) {
        $srv = dns_get_record($serviceName, DNS_SRV);

        $hosts = array_map(function ($record) {
            return $record["target"];
        }, $srv);

        sort($hosts);

        if ($ipOnly) {
            $srvHosts = $hosts;

            $hosts = array_map(function ($target) {
                $r = dns_get_record($target, DNS_A);
                return $r[0]['ip'];
            }, $srvHosts);
        }

        apcu_add("$serviceName-hosts", $hosts, 60);
    }

    return $hosts;
}

function config_getSecrets()
{
    $secretsInCache = false;

    $secrets = apcu_fetch("mw-secrets", $secretsInCache);

    if (!$secretsInCache) {
        $secrets = [];

        $files = scandir('/opt/mediawiki-secrets');
        $secretFiles = array_filter($files, function ($fileName) {
            return !(substr($fileName, 0, 1) === '.');
        });

        foreach ($secretFiles as $fileName) {
            $secrets[$fileName] = file_get_contents("/opt/mediawiki-secrets/$fileName");
        }

        apcu_add("mw-secrets", $secrets, 600);
    }

    return $secrets;
}

function config_generateFileBackend(string $wiki, string $key, string $secret, string $region, bool $sharded, string $bucketBase = null)
{
    if ($bucketBase === null) {
        $bucketBase = $wiki;
    }
    $config = [
        'name' => 'AmazonS3',
        'class' => 'AmazonS3FileBackend',
        'awsKey' => $key,
        'awsSecret' => $secret,
        'awsRegion' => $region,
        'endpoint' => "https://$region.digitaloceanspaces.com",
        'lockManager' => 'nullLockManager',
        'containerPaths' => [
            "$wiki-local-deleted" => "$bucketBase-deleted",
            "$wiki-local-temp" => "$bucketBase-temp"
        ]
    ];
    if ($sharded) {
        $config['shardViaHashLevels'] = [
            'local-public' => ['levels' => 1, 'base' => 16, 'repeat' => true],
            'local-thumb' => ['levels' => 1, 'base' => 16, 'repeat' => true]
        ];

        foreach (range(0, 15) as &$i) {
            $suffix = base_convert($i, 10, 16);
            $config['containerPaths']["$wiki-local-public.$suffix"] = "$bucketBase-$suffix";
            $config['containerPaths']["$wiki-local-thumb.$suffix"] = "$bucketBase-thumbs-$suffix";
        }
    } else {
        $config['containerPaths']["$wiki-local-public"] = "$bucketBase";
        $config['containerPaths']["$wiki-local-thumb"] = "$bucketBase-thumbs";
    }

    return ['s3' => $config];
}

function config_generateLocalFileRepo(string $rootDomain)
{
    $config = [
        'class' => 'LocalRepo',
        'name' => 'local',
        'backend' => 'AmazonS3',
        'url' => '/w/img_auth.php',
        'hashLevels' => 2,
        'deletedHashLevels' => 2,
        'transformVia404' => true,
        'zones' => [
            'public' => [
                'url' => "https://images.$rootDomain"
            ],
            'thumb' => [
                'url' => "https://thumbnails.$rootDomain"
            ],
            'deleted' => [
                'url' => false
            ],
            'temp' => [
                'url' => false
            ]
        ]
    ];

    return $config;
}

function config_configureNamespaces(array $nsconfig)
{
    global $wgExtraNamespaces, $wgNamespacesWithSubpages, $wgNamespaceProtection;
    foreach ($nsconfig as $ns => $settings) {
        $id = $settings['id'];
        define($ns, $id);
        $wgExtraNamespaces[$id] = $settings['name'];
        $wgNamespacesWithSubpages[$id] = true;
        if (isset($settings['protection'])) {
            $permissionName = $settings['protection'];
            $wgNamespaceProtection[$id] = [$permissionName];
        }
    }
}

// For PHP Constants to be usable with custom namespaces, we first need to define them before the $wgConf->settings array is set.
$pcgwDefineNamespacesID = [
        "NS_MODULE"           => 828,
        "NS_MODULE_TALK"      => 829,
        "NS_DEVELOPMENT"      => 400,
        "NS_DEVELOPMENT_TALK" => 401,
        "NS_SERIES"           => 402,
        "NS_SERIES_TALK"      => 403,
        "NS_ENGINE"           => 404,
        "NS_ENGINE_TALK"      => 405,
        "NS_GUIDE"            => 406,
        "NS_GUIDE_TALK"       => 407,
        "NS_EMULATION"        => 410,
        "NS_EMULATION_TALK"   => 411,
        "NS_CONTROLLER"       => 412,
        "NS_CONTROLLER_TALK"  => 413,
        "NS_GLOSSARY"         => 414,
        "NS_GLOSSARY_TALK"    => 415,
        "NS_COMPANY"          => 416,
        "NS_COMPANY_TALK"     => 417,
        "NS_STORE"            => 418,
        "NS_STORE_TALK"       => 419
];

foreach ($pcgwDefineNamespacesID as $ns => $id) {
    define($ns, $id);
}


/*
 * We have to set a couple of default permissions up here because MediaWiki's SiteConfiguration object makes `true`
 * values take precedence when merging arrays.
 * See: https://doc.wikimedia.org/mediawiki-core/master/php/SiteConfiguration_8php_source.html#l00595
 */
$wgGroupPermissions['*']['skipcaptcha'] = false;
$wgGroupPermissions['*']['createaccount'] = false;
$wgGroupPermissions['*']['createpage'] = false;
$wgGroupPermissions['*']['createtalk'] = false;
$wgGroupPermissions['*']['edit'] = false;
$wgGroupPermissions['*']['flow-hide'] = false;
$wgGroupPermissions['user']['skipcaptcha'] = false;
$wgGroupPermissions['user']['createpage'] = false;
$wgGroupPermissions['user']['upload'] = false;
$wgGroupPermissions['user']['reupload'] = false;
$wgGroupPermissions['user']['reupload-shared'] = false;
$wgGroupPermissions['user']['move'] = false;
$wgGroupPermissions['user']['movefile'] = false;
$wgGroupPermissions['user']['move-subpages'] = false;
$wgGroupPermissions['user']['move-categorypages'] = false;
$wgGroupPermissions['user']['sendemail'] = false;
$wgGroupPermissions['user']['move-rootuserpages'] = false;
$wgGroupPermissions['user']['flow-lock'] = false; // Extension:StructuredDiscussions ->  Mark Structured Discussions topics as resolved
$wgGroupPermissions['user']['createclass'] = false; // Extension:Page Forms -> Special:CreateClass
$wgGroupPermissions['user']['multipageedit'] = false; // Extension:Page Forms -> Special:MultiPageEdit
$wgGroupPermissions['user']['spamblacklistlog'] = false; // Extension:SpamBlacklist -> Special:Log/spamblacklist

$pcgwSecrets = config_getSecrets();
$mysqlHosts = config_getHosts('mysql');

/** @noinspection PhpUndefinedVariableInspection */
$wgConf->settings = [
    'wgSitename' => [
        'ftlwiki' => 'FTL Wiki',
        'gunpointwiki' => 'Gunpoint Wiki',
        'prisonarchitectwiki' => 'Prison Architect Wiki',
        'siryouarebeinghuntedwiki' => 'Sir, You Are Being Hunted Wiki',
        'pcgamingwiki' => 'PCGamingWiki',
        'applegamingwiki' => 'AppleGamingWiki'
    ],
    // System configuration
    'wgDBservers' => [
        'default' => array_map(function (string $host, $idx) {
            global $pcgwSecrets;
            /** @noinspection PhpUndefinedConstantInspection */
            return [
                'host' => $host,
                'user' => 'mediawiki',
                'password' => $pcgwSecrets["DB_PASSWORD"],
                'type' => 'mysql',
                'flags' => DBO_DEFAULT,
                'load' => 1,
                'useGTIDs' => true,
                'lagDetectionMethod' => 'Seconds_Behind_Master',
                'max lag' => '30'
            ];
        }, $mysqlHosts, array_keys($mysqlHosts))
    ],
    'wgDBTableOptions' => [
        'default' => 'ENGINE=InnoDB, DEFAULT CHARSET=binary'
    ],
    'wgLanguageCode' => [
        'default' => 'en'
    ],
    'wgShellLocale' => [
        'default' => 'en_US.utf8'
    ],
    // Page caching
    'wgUseCdn' => [
        'default' => true
    ],
    'wgCdnServers' => [
        'default' => array_map(function (string $host) {
            return "$host:8080";
        }, config_getHosts('trafficserver', true))
    ],
    'wgCdnServersNoPurge' => [
        'default' => ['10.0.0.0/8']
    ],
    'wgUsePrivateIPs' => [
        'default' => false
    ],
    'wgUseETag' => [
        'default' => true
    ],
    // Object caching
    '+wgObjectCaches' => [
        'default' => [
            'memcached-pecl' => [
                'servers' => array_map(function (string $host) {
                    return "$host:11211";
                }, config_getHosts('memcached'))
            ]
        ]
    ],
    'wgMainCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgParserCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgMessageCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'wgSessionCacheType' => [
        'default' => CACHE_DB
    ],
    'wgUseLocalMessageCache' => [
        'default' => true
    ],
    'wgCacheDirectory' => [
        'default' => "$IP/cache"
    ],
    'wgEnableSidebarCache' => [
        'default' => true
    ],
    'wgParserCacheExpireTime' => [
        'default' => '86400'
    ],
    // Performance
    'wgJobRunRate' => [
        'default' => 1,
        'pcgamingwiki' => 0
    ],
    /* Disabled 2022-12-04 since the feature was removed completely in version 1.35.0.
    'wgDisableCounters' => [
        'default' => true
    ],
    */
    'wgMiserMode' => [
        'default' => true
    ],
    'wgCategoryPagingLimit' => [
        'default' => 9600
    ],
    // URL settings
    'wgServer' => [
        'ftlwiki' => 'https://www.ftlwiki.com',
        'gunpointwiki' => 'https://www.gunpointwiki.net',
        'prisonarchitectwiki' => 'https://www.prisonarchitectwiki.com',
        'siryouarebeinghuntedwiki' => 'https://www.siryouarebeinghuntedwiki.com',
        'pcgamingwiki' => 'https://www.pcgamingwiki.com',
        'applegamingwiki' => 'https://www.applegamingwiki.com'
    ],
    'wgInternalServer' => [
        'pcgamingwiki' => 'http://www.pcgamingwiki.com'
    ],
    'wgScriptPath' => [
        'default' => '/w'
    ],
    'wgArticlePath' => [
        'default' => '/wiki/$1'
    ],
    'wgUsePathInfo' => [
        'default' => true
    ],
    '+wgUrlProtocols' => [
        'default' => ['steam://']
    ],
    // Email settings
    'wgEnableEmail' => [
        'default' => true
    ],
    'wgEnableUserEmail' => [
        'default' => true // Enables user-to-user e-mails through Special:EmailUser, requires the 'sendemail' privileged (not granted to users on PCGW sites)
    ],
    'wgEnotifUserTalk' => [
        'default' => false // This is not necessary since it is handled through Extension:StructuredDiscussions
    ],
    'wgEnotifWatchlist' => [
        'default' => true // Allow users to opt-in to mail notifications for watched pages
    ],
    'wgEnotifMinorEdits' => [
        'default' => true // Allow users to opt-in to mail notifications for minor edits on watched pages
    ],
    'wgEmailAuthentication' => [
        'default' => true
    ],
    'wgEmergencyContact' => [
        'default' => 'contact@pcgamingwiki.com'
    ],
    'wgPasswordSender' => [
        'default' => 'noreply@wiki.pcgamingwiki.com'
    ],
    'wgNoReplyAddress' => [
        'default' => 'noreply@wiki.pcgamingwiki.com'
    ],
    // File settings
    'wgEnableUploads' => [
        'default' => true
    ],
    'wgUseInstantCommons' => [
        'default' => false
    ],
    'wgUseImageMagick' => [
        'default' => true
    ],
    'wgImageMagickConvertCommand' => [
        'default' => '/usr/bin/convert'
    ],
    'wgJpegQuality' => [
        'default' => 95 // Quality used to generate JPEG thumbnails (1-100) -- defaults to 80.
    ],
    'wgAllowTitlesInSVG' => [
        'default' => true
    ],
    '+wgFileExtensions' => [
        'default' => ['svg']
    ],
    'wgSVGConverter' => [
        'default' => 'rsvg'
    ],
    'wgMaxUploadSize' => [
        'default' => 1024 * 1024 * 16
    ],
    'wgUploadSizeWarning' => [
        'default' => 1024 * 1024 * 16
    ],
    // File storage
    'wgFileBackends' => [
        'ftlwiki' => config_generateFileBackend('ftlwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false),
        'gunpointwiki' => config_generateFileBackend('gunpointwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false),
        'prisonarchitectwiki' => config_generateFileBackend('prisonarchitectwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false),
        'siryouarebeinghuntedwiki' => config_generateFileBackend('siryouarebeinghuntedwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false),
        'pcgamingwiki' => config_generateFileBackend('pcgamingwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false, "pcgw"),
        'applegamingwiki' => config_generateFileBackend('applegamingwiki', $pcgwSecrets["AWS_KEY"], $pcgwSecrets["AWS_SECRET"], "nyc3", false)
    ],
    'wgLocalFileRepo' => [
        'ftlwiki' => config_generateLocalFileRepo('ftlwiki.com'),
        'gunpointwiki' => config_generateLocalFileRepo('gunpointwiki.net'),
        'prisonarchitectwiki' => config_generateLocalFileRepo('prisonarchitectwiki.com'),
        'siryouarebeinghuntedwiki' => config_generateLocalFileRepo('siryouarebeinghuntedwiki.com'),
        'pcgamingwiki' => config_generateLocalFileRepo('pcgamingwiki.com'),
        'applegamingwiki' => config_generateLocalFileRepo('applegamingwiki.com')
    ],
    // Authentication & Authorization
    'wgAutoConfirmAge' => [
        'default' => 21600
    ],
    'wgAutoConfirmCount' => [
        'default' => 3
    ],
    'wgAutopromote' => [
        'default' => [
            "autoconfirmed" => ["&",
                [APCOND_EDITCOUNT, &$wgAutoConfirmCount],
                [APCOND_AGE, &$wgAutoConfirmAge],
            ],
            "autotrusted" => ["&",
                [APCOND_EDITCOUNT, 500],
                [APCOND_AGE, 180*86400],
            ],
            "emailconfirmed" => APCOND_EMAILCONFIRMED,
        ]
    ],
    '+wgNamespaceProtection' => [
        'default' => [
            NS_TEMPLATE    => 'edittemplate',
            NS_PROJECT     => 'editproject'
        ]
    ],
    /* Disable Special:ResetPassword
    'wgPasswordResetRoutes' => [
        'default' => [
            'username' => false,
            'email'    => false
        ]
    ],
    */
    // Add custom protection levels (and rearrange the default ones)
    'wgRestrictionLevels' => [
        'default' => [
            '',
            'autoconfirmed',
            'editsemiprotected-trustedorabove', // Meant to protect a page to Trusted or above
            'editsemiprotected-editorsorabove', // Meant to protect a page to Editors or above
            'sysop'
        ]
    ],
    'wgCascadingRestrictionLevels' => [
        'default' => [
            'editsemiprotected-editorsorabove', // Meant to protect a page and its cascading pages to Editors or above
            'sysop'
        ]
    ],
    // This allows bots/tools which have been given the 'editpage' grant to also edit custom namespaces, see Special:ListGrants
    '+wgGrantPermissions' => [
        '+pcgamingwiki' => [
            'editpage' => [
                'editseries' => true,
                'editengine' => true,
                'editemulation' => true,
                'editcontroller' => true,
                'editglossary' => true,
                'editcompany' => true,
                'editstore' => true,
                'editguide' => true
            ]
        ]
    ],
    // Main user group rights
    '+wgGroupPermissions' => [
        'default' => [
            '*' => [ // Disallow all forms of editing for anonymous users, even on talk pages
                'skipcaptcha' => false,
                'autocreateaccount' => true,
                'createaccount' => false,
                'createpage' => false,
                'createtalk' => false,
                'edit' => false,
                'flow-hide' => false,
                'runcargoqueries' => false
            ],
            'bot' => [ // Bots should always skip captchas
                'skipcaptcha' => true
            ],
            'user' => [ // New users can create new talk pages and edit existing pages
                'skipcaptcha' => false,
                'createpage' => false,
                'createtalk' => true,
                'upload' => false,
                'reupload' => false,
                'reupload-shared' => false,
                'edit' => true,
                'move' => false,
                'movefile' => false,
                'move-subpages' => false,
                'move-categorypages' => false,
                'move-rootuserpages' => false,
                'sendemail' => false,
                'flow-lock' => false, // Extension:StructuredDiscussions ->  Mark Structured Discussions topics as resolved
                'createclass' => false, // Extension:Page Forms -> Special:CreateClass
                'multipageedit' => false, // Extension:Page Forms -> Special:MultiPageEdit
                'spamblacklistlog' => false, // Extension:SpamBlacklist -> Special:Log/spamblacklist
                'runcargoqueries' => true
            ],
            'autoconfirmed' => [ // Auto-confirmed users can create new pages and upload new files/images
                'skipcaptcha' => false,
                'createpage' => true,
                'upload' => true,
                'reupload' => true,
                'reupload-shared' => true,
                'move' => true,
                'movefile' => true,
                'move-subpages' => true,
                'move-categorypages' => true,
                'flow-lock' => true
            ],
            'emailconfirmed' => [ // Never actually used
                'skipcaptcha' => false
            ],
            'autotrusted' => [ // Auto trusted users can suppress redirects and skip captcha checks
                'skipcaptcha' => true,
                'suppressredirect' => true,
                'editsemiprotected-trustedorabove' => true
            ],
            'Trusted' => [ // Trusted users can suppress redirects and skip captcha checks
                'skipcaptcha' => true,
                'suppressredirect' => true,
                'editsemiprotected-trustedorabove' => true,
                'runcargoqueries' => true
            ],
            'editor' => [ // Editors can edit the Project namespace (aka PCGamingWiki/AppleGamingWiki namespaces), as well as hide talk posts
                'skipcaptcha' => true,
                'ipblock-exempt' => true,
                'noratelimit' => true,
                'suppressredirect' => true,
                'rollback' => true,
                'editproject' => true,
                'move-rootuserpages' => true,
                'createclass' => true, // Extension:Page Forms -> Special:CreateClass
                'multipageedit' => true, // Extension:Page Forms -> Special:MultiPageEdit
                'protect' => true, // Allow editors to change protection on pages as well
                'delete' => true, // Allow editors to remove pages
                'bigdelete' => false, // Do NOT allow editors to remove pages with thousands of edits
                'undelete' => true, // Also allow editors to restore pages
                'deletedhistory' => true, // Required to allow editors to restore pages
                'flow-hide' => true,
                'editsemiprotected-trustedorabove' => true,
                'editsemiprotected-editorsorabove' => true,
                'patrol' => true,
                'autopatrol' => true,
                'runcargoqueries' => true
            ],
            'template-editor' => [ // Template Editors can edit templates, modules, and development namespace
                'skipcaptcha' => true,
                'edittemplate' => true,
                'editdevelopment' => true,
                'editsemiprotected-trustedorabove' => true,
                'editsemiprotected-editorsorabove' => true
            ],
            'moderator' => [ // Moderators can manage users and pages
                'skipcaptcha' => true,
                'ipblock-exempt' => true,
                'noratelimit' => true,
                'suppressredirect' => true,
                'rollback' => true,
                'editproject' => true,
                'block' => true,
                'blockemail' => true,
                'checkuser' => true,
                'move-rootuserpages' => true,
                'createclass' => true, // Extension:Page Forms -> Special:CreateClass
                'multipageedit' => true, // Extension:Page Forms -> Special:MultiPageEdit
                'protect' => true,
                'usermerge' => true,
                'usermerge' => true,
                'delete' => true,
                'bigdelete' => true,
                'deletelogentry' => true,
                'deleterevision' => true,
                'mergehistory' => true,
                'browsearchive' => true,
                'undelete' => true,
                'deletedhistory' => true,
                'deletedtext' => true,
                'flow-create-board' => true,
                'flow-edit-post' => true,
                'flow-hide' => true,
                'flow-delete' => true,
                'flow-suppress' => true,
                'spamblacklistlog' => true, // Extension:SpamBlacklist -> Special:Log/spamblacklist
                'editsemiprotected-trustedorabove' => true,
                'editsemiprotected-editorsorabove' => true,
                'patrol' => true,
                'autopatrol' => true
            ],
            'sysop' => [ // Administrators can do various things related to user permissions and templates
                'skipcaptcha' => true,
                'usermerge' => true,
                'deletelogentry' => true,
                'deleterevision' => true,
                'interwiki' => true,
                'edittemplate' => true,
                'editproject' => true,
                'editwidgets' => true,
                'flow-create-board' => true,
                'flow-hide' => true,
                'spamblacklistlog' => true, // Extension:SpamBlacklist -> Special:Log/spamblacklist
                'editsemiprotected-trustedorabove' => true,
                'editsemiprotected-editorsorabove' => true,
                'patrol' => true,
                'autopatrol' => true,
                'runcargoqueries' => true
            ],
            'bureaucrat' => [ // Bureaucrats (aka Super Administrators) mostly uses the default configured permissions (e.g. edit user rights etc)
                'skipcaptcha' => true,
                'interwiki' => true,
                'usermerge' => true,
                'flow-hide' => true,
                'editsemiprotected-trustedorabove' => true,
                'editsemiprotected-editorsorabove' => true,
                'autopatrol' => true,
                'sendemail' => true // Used for testing SMTP sending
            ]
        ],
        '+applegamingwiki' => [
            'user' => [ // Unique for AGW is that users don't need to be autoconfirmed before they can create new pages or uploads new files/images
                'createpage' => true,
                'upload' => true
            ]
        ],
        '+pcgamingwiki' => [
            '*' => [ // For PCGW we also need to disallow all edits of the custom namespaces
                'editseries' => false,
                'editengine' => false,
                'editemulation' => false,
                'editcontroller' => false,
                'editglossary' => false,
                'editcompany' => false,
                'editstore' => false,
                'editguide' => false
            ],
            'autoconfirmed' => [ // Similarly, we also need to allow custom namespace edits for autoconfirmed users
                'editseries' => true,
                'editengine' => true,
                'editemulation' => true,
                'editcontroller' => true,
                'editglossary' => true,
                'editcompany' => true,
                'editstore' => true,
                'editguide' => true
            ],
            'sysop' => [ // The Development namespace is restricted to Administrators
                'editdevelopment' => true
            ]
        ]
    ],
    'wgPluggableAuth_ButtonLabel' => [
        'default' => 'Login with PCGamingWiki Account'
    ],
    'wgPluggableAuth_EnableLocalLogin' => [
        'pcgamingwiki' => false
    ],
    'wgPluggableAuth_EnableLocalProperties' => [
        'default' => true
    ],
    'wgOpenIDConnect_Config' => [
        'default' => [
            'https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/' => [
                'name' => 'PCGamingWiki Account',
                'clientID' => 'mediawiki',
                'clientsecret' => $pcgwSecrets["OIDC_SECRET"],
                'scope' => ['openid', 'profile', 'email']
            ]
        ]
    ],
    'wgOpenIDConnect_MigrateUsersByEmail' => [
        'default' => true
    ],
    'wgOpenIDConnect_ForceLogout' => [
        'default' => false
    ],
    // UX
    'wgAddPersonalUrlsTable' => [
        'default' => [
            'pcgw-account' => 'https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/account'
        ]
    ],
    'wgHiddenPrefs' => [
        'default' => ['skin']
    ],
    'wgAllowUserCss' => [
        'default' => true
    ],
    'wgAllowUserJs' => [
        'default' => true
    ],
    'wgAllowSiteCSSOnRestrictedPages' => [
        'default' => true
    ],
    'wgResourceLoaderMaxQueryLength' => [
        'default' => -1
    ],
    '+wgDefaultUserOptions' => [
        'default' => [
            'usebetatoolbar' => 1,
            'usebetatoolbar-cgd' => 1,
            'wikieditor-preview' => 1,
            'numberheadings' => 0
        ],
        '+pcgamingwiki' => [
            'wikieditor-preview' => 0
        ]
    ],
    'wgLogo' => [
        'ftlwiki' => 'https://static.pcgamingwiki.com/logos/ftlwiki.png',
        'gunpointwiki' => 'https://static.pcgamingwiki.com/logos/gunpointwiki.png',
        'prisonarchitectwiki' => 'https://static.pcgamingwiki.com/logos/prisonarchitectwiki.png',
        'siryouarebeinghuntedwiki' => 'https://static.pcgamingwiki.com/logos/siryouarebeinghuntedwiki.png',
        'pcgamingwiki' => 'https://static.pcgamingwiki.com/logos/pcgamingwiki.svg',
        'applegamingwiki' => 'https://pcgw-static.nyc3.digitaloceanspaces.com/logos/AppleGamingWiki.svg'
    ],
    'wgFavicon' => [
        'ftlwiki' => 'https://static.pcgamingwiki.com/favicons/ftlwiki.png',
        'gunpointwiki' => 'https://static.pcgamingwiki.com/favicons/gunpointwiki.png',
        'pcgamingwiki' => 'https://static.pcgamingwiki.com/favicons/pcgamingwiki.png',
        'applegamingwiki' => 'https://static.pcgamingwiki.com/favicons/applegamingwiki.png'
    ],
    'wgDefaultSkin' => [
        'ftlwiki' => 'pcgwnvector',
        'gunpointwiki' => 'pcgwnvector',
        'prisonarchitectwiki' => 'pcgwnvector',
        'siryouarebeinghuntedwiki' => 'pcgwnvector',
        'pcgamingwiki' => 'overclocked',
        'applegamingwiki' => 'overclocked'
    ],
    // Licensing
    'wgRightsUrl' => [
        'default' => '//creativecommons.org/licenses/by-nc-sa/3.0'
    ],
    'wgRightsText' => [
        'default' => 'Creative Commons Attribution Non-Commercial Share Alike'
    ],
    '+wgFooterIcons' => [
        'default' => [
            '+poweredby' => [
                'myicon' => [
                    "src" => "https://static.pcgamingwiki.com/images/PoweredBy.png",
                    "url" => "https://pcgamingwiki.com/",
                    "alt" => "Brought to you by PCGamingWiki",
                    "height" => "31",
                    "width" => "100",
                ]
            ]
        ]
    ],
    // Flow
    'wgNamespaceContentModels' => [
        'default' => [
            NS_TALK            => 'flow-board',
            NS_USER_TALK       => 'flow-board',
            NS_PROJECT_TALK    => 'flow-board',
            NS_FILE_TALK       => 'flow-board',
            NS_MEDIAWIKI_TALK  => 'flow-board',
            NS_TEMPLATE_TALK   => 'flow-board',
            NS_HELP_TALK       => 'flow-board',
            NS_CATEGORY_TALK   => 'flow-board'
        ],
        '+pcgamingwiki' => [
            NS_SERIES_TALK     => 'flow-board',  // NS_SERIES_TALK     (403)
            NS_ENGINE_TALK     => 'flow-board',  // NS_ENGINE_TALK     (405)
            NS_GUIDE_TALK      => 'flow-board',  // NS_GUIDE_TALK      (407)
            NS_EMULATION_TALK  => 'flow-board',  // NS_EMULATION_TALK  (411)
            NS_CONTROLLER_TALK => 'flow-board',  // NS_CONTROLLER_TALK (413)
            NS_GLOSSARY_TALK   => 'flow-board',  // NS_GLOSSARY_TALK   (415)
            NS_COMPANY_TALK    => 'flow-board',  // NS_COMPANY_TALK    (417)
            NS_STORE_TALK      => 'flow-board'   // NS_STORE_TALK      (419)
        ]
    ],
    'wgFlowContentFormat' => [
        'default' => 'wikitext'
    ],
    // Misc
    'wgLqtTalkPages' => [
        'default' => false
    ],
    // Captcha
    'wgCaptchaClass' => [
        'default' => 'ReCaptchaNoCaptcha'
    ],
    'wgReCaptchaSiteKey' => [
        'default' => $pcgwSecrets["RECAPTCHA_SITEKEY"]
    ],
    'wgReCaptchaSecretKey' => [
        'default' => $pcgwSecrets["RECAPTCHA_SECRETKEY"]
    ],
    'wgCaptchaTriggers' => [
        'default' => [
            'edit' => false,
            'create' => false,
            'addurl' => false, // 2023-02-28, disabled entirely cuz disabling it just on the talk pages (below) doesn't seem to work as expected...
            'createaccount' => true,
            'badlogin' => true
        ]
    ],
    // Experimental
    'wgCaptchaTriggersOnNamespace' => [ // Captchas apparently fail on talk pages when adding links, so lets disable it entirely there 
        'default' => [ // Doesn't work as expected...
            NS_TALK             => ['addurl' => false ],
            NS_USER_TALK        => ['addurl' => false ],
            NS_PROJECT_TALK     => ['addurl' => false ],
            NS_FILE_TALK        => ['addurl' => false ],
            NS_MEDIAWIKI_TALK   => ['addurl' => false ],
            NS_TEMPLATE_TALK    => ['addurl' => false ],
            NS_HELP_TALK        => ['addurl' => false ],
            NS_CATEGORY_TALK    => ['addurl' => false ]
        ],
        '+pcgamingwiki' => [ // Doesn't work as expected...
            NS_SERIES_TALK      => ['addurl' => false ],
            NS_ENGINE_TALK      => ['addurl' => false ],
            NS_GUIDE_TALK       => ['addurl' => false ],
            NS_EMULATION_TALK   => ['addurl' => false ],
            NS_CONTROLLER_TALK  => ['addurl' => false ],
            NS_GLOSSARY_TALK    => ['addurl' => false ],
            NS_COMPANY_TALK     => ['addurl' => false ],
            NS_STORE_TALK       => ['addurl' => false ]
        ]
    ],
    // Analytics
    'wgGoogleAnalyticsAccount' => [
        'default' => '',
        'ftlwiki' => 'UA-7268820-10',
        'gunpointwiki' => 'UA-7268820-20',
        'prisonarchitectwiki' => 'UA-7268820-14',
        'siryouarebeinghuntedwiki' => 'UA-7268820-25',
        'pcgamingwiki' => 'UA-7268820-8',
        'applegamingwiki' => 'G-R3D95LP260'
    ],
    'wgMatomoURL' => [
        'default' => 'piwik.pcgamingwiki.com'
    ],
    'wgMatomoIDSite' => [
        'ftlwiki' => '6',
        'gunpointwiki' => '36',
        'prisonarchitectwiki' => '30',
        'siryouarebeinghuntedwiki' => '41',
        'pcgamingwiki' => '1',
        'applegamingwiki' => '53'
    ],
    // Extension loading
    'pcgwLoadExtensions' => [
        'default' => [
            'Auth_remoteuser',
            'AWS',
            'CheckUser',
            'Cite',
            'ConfirmEdit',
            'ConfirmEdit/ReCaptchaNoCaptcha',
            'Echo',
            'Flow',
            'googleAnalytics',
            'HTMLets',
            'intersection',
            'Interwiki',
            'Matomo',
            'MultimediaViewer',
            'Nuke',
            'KeycloakAuth',
            'PageForms',
            'ParserFunctions',
            'Renameuser',
            'ReplaceText',
            'Scribunto',
            'SpamBlacklist',
            'SubpageFun',
            'Thanks',
            //'TitleBlacklist', // Disabled 2022-12-04 due to doubt it is even being used/relevant
            'TitleKey', // Having this enabled breaks search suggestions in other namespaces (see https://www.mediawiki.org/wiki/Topic:V5nrescqi2o16enh ), but it is required for case insensitive suggestions to work in the main namespace
            'UserMerge',
            'Variables',
            'WikiEditor',
        ],
        '+pcgamingwiki' => [
            'Cargo',
            //'ExternalData',
            'Widgets'
        ],
        '+applegamingwiki' => [
            'Cargo',
            'Widgets'
        ]
    ],
    'pcgwLoadSkins' => [
        'default' => [
            'Vector',
            'pcgwnvector'
        ],
        'pcgamingwiki' => [
            'overclocked'
        ],
        'applegamingwiki' => [
            'overclocked'
        ]
    ],
    /* Disabled 2022-12-04 due to Extension:Semantic MediaWiki not being used
    'pcgwSemanticsDomain' => [
        'default' => null,
        'ftlwiki' => 'ftlwiki.com',
        'pcgamingwiki' => 'pcgamingwiki.com'
    ],
    */
    // Namespace stuff
    'wgNamespacesWithSubpages' => [
        '+pcgamingwiki' => array_fill(0, 500, true)
    ],
    'pcgwDefineNamespaces' => [
        'default' => [
            "NS_MODULE" => ['id' => 828, 'name' => 'Module', 'protection' => 'edittemplate'],
            "NS_MODULE_TALK" => ['id' => 829, 'name' => 'Module_talk', 'protection' => 'edittemplate']
        ],
        '+pcgamingwiki' => [
            "NS_DEVELOPMENT" => ['id' => 400, 'name' => 'Development', 'protection' => 'editdevelopment'],
            "NS_DEVELOPMENT_TALK" => ['id' => 401, 'name' => 'Development_talk'],
            "NS_SERIES" => ['id' => 402, 'name' => 'Series', 'protection' => 'editseries'],
            "NS_SERIES_TALK" => ['id' => 403, 'name' => 'Series_talk'],
            "NS_ENGINE" => ['id' => 404, 'name' => 'Engine', 'protection' => 'editengine'],
            "NS_ENGINE_TALK" => ['id' => 405, 'name' => 'Engine_talk'],
            "NS_GUIDE" => ['id' => 406, 'name' => 'Guide', 'protection' => 'editguide'],
            "NS_GUIDE_TALK" => ['id' => 407, 'name' => 'Guide_talk'],
            "NS_EMULATION" => ['id' => 410, 'name' => 'Emulation', 'protection' => 'editemulation'],
            "NS_EMULATION_TALK" => ['id' => 411, 'name' => 'Emulation_talk'],
            "NS_CONTROLLER" => ['id' => 412, 'name' => 'Controller', 'protection' => 'editcontroller'],
            "NS_CONTROLLER_TALK" => ['id' => 413, 'name' => 'Controller_talk'],
            "NS_GLOSSARY" => ['id' => 414, 'name' => 'Glossary', 'protection' => 'editglossary'],
            "NS_GLOSSARY_TALK" => ['id' => 415, 'name' => 'Glossary_talk'],
            "NS_COMPANY" => ['id' => 416, 'name' => 'Company', 'protection' => 'editcompany'],
            "NS_COMPANY_TALK" => ['id' => 417, 'name' => 'Company_talk'],
            "NS_STORE" => ['id' => 418, 'name' => 'Store', 'protection' => 'editstore'],
            "NS_STORE_TALK" => ['id' => 419, 'name' => 'Store_talk']
        ]
    ],
    'wgNamespacesToBeSearchedDefault' => [
        'default' => [
            NS_MAIN           => true,
            NS_TALK           => false,
            NS_USER           => false,
            NS_USER_TALK      => false,
            NS_PROJECT        => false,
            NS_PROJECT_TALK   => false,
            NS_IMAGE          => false,
            NS_IMAGE_TALK     => false,
            NS_MEDIAWIKI      => false,
            NS_MEDIAWIKI_TALK => false,
            NS_TEMPLATE       => false,
            NS_TEMPLATE_TALK  => false,
            NS_HELP           => false,
            NS_HELP_TALK      => false,
            NS_CATEGORY       => false,
            NS_CATEGORY_TALK  => false
        ],
        '+pcgamingwiki' => [
            NS_SERIES           => true,
            NS_SERIES_TALK      => false,
            NS_ENGINE           => true,
            NS_ENGINE_TALK      => false,
            NS_COMPANY          => true,
            NS_COMPANY_TALK     => false,
            NS_GLOSSARY         => true,
            NS_GLOSSARY_TALK    => false,
            NS_STORE            => true,
            NS_STORE_TALK       => false,
            NS_GUIDE            => true,
            NS_GUIDE_TALK       => false
        ]
    ],
/*
    'pcgwDefaultSearchNamespaces' => [
        'pcgamingwiki' => [
            'NS_MAIN' => true,
            'NS_TALK' => true,
            'NS_SERIES' => true,
            'NS_SERIES_TALK' => false,
            'NS_ENGINE' => true,
            'NS_ENGINE_TALK' => false,
            'NS_COMPANY' => true,
            'NS_COMPANY_TALK' => false,
            'NS_GLOSSARY' => true,
            'NS_GLOSSARY_TALK' => false,
            'NS_STORE' => true,
            'NS_STORE_TALK' => false,
            'NS_GUIDE' => true,
            'NS_GUIDE_TALK' => false,
            'NS_USER' => false,
            'NS_USER_TALK' => false,
            'NS_PROJECT' => false,
            'NS_PROJECT_TALK' => false,
            'NS_IMAGE' => false,
            'NS_IMAGE_TALK' => false,
            'NS_MEDIAWIKI' => false,
            'NS_MEDIAWIKI_TALK' => false,
            'NS_TEMPLATE' => false,
            'NS_TEMPLATE_TALK' => false,
            'NS_HELP' => false,
            'NS_HELP_TALK' => false,
            'NS_CATEGORY' => false,
            'NS_CATEGORY_TALK' => false
        ]
    ],
*/
/*
    'pcgwSemanticLinkNamespaces' => [
        'pcgamingwiki' => [
            'NS_SERIES' => true,
            'NS_ENGINE' => true,
            'NS_COMPANY' => true,
            'NS_USER' => false,
            'NS_PROJECT' => false,
            'NS_GUIDE' => false,
            'NS_GLOSSARY => false'
        ]
    ],
    // Semantic MediaWiki
    '$smwgEnabledCompatibilityMode' => [
        'default' => true
    ],
    'smwgQMaxInlineLimit' => [
        'default' => 5000
    ],
    'smwgQMaxLimit' => [
        'default' => 50000
    ],
    'smwgQUpperbound' => [
        'default' => 50000
    ],
    'smwgEnableUpdateJobs' => [
        'default' => true
    ],
    'smwgQueryResultCacheType' => [
        'default' => 'memcached-pecl'
    ],
    'smwgCacheType' => [
        'default' => 'memcached-pecl'
    ],
//    'smwgQTemporaryTablesAutoCommitMode' => [
//        'default' => true
//    ],
    'smwgLocalConnectionConf' => [
        'default' => [
            'mw.db' => array(
                'read'  => DB_REPLICA,
                'write' => DB_MASTER
            ),
            'mw.db.queryengine' => array(
                'read'  => DB_MASTER,
                'write' => DB_MASTER
            )
        ]
    ],
    'smwgEnabledEditPageHelp' => [
        'default' => false
    ],
*/
    // Cargo
    'wgCargoDBname' => [
        'pcgamingwiki' => 'pcgamingwiki_cargo',
        'applegamingwiki' => 'applegamingwiki_cargo'
    ],
    'wgCargoDBuser' => [
        'default' => 'mediawiki_cargo'
    ],
    'wgCargoDBpassword' => [
        'default' => $pcgwSecrets["CARGO_DB_PASSWORD"]
    ],
    'wgCargoAllowedSQLFunctions' => [
        'default' => [
            'RAND'
        ]
    ],
    'wgCargoDBserver' => [
        'default' => config_getHosts('mysql')[0]
    ],
    // External Data
    /* Disabled 2022-12-04 (Extension:External Data is not being used)
    'edgAllowExternalDataFrom' => [
        'default' => [
            "https://api.isthereanydeal.com/",
            "https://piwik.pcgamingwiki.com/",
            "http://m.co-optimus.com"
        ]
    ],
    'edgStringReplacements' => [
        'default' => [
            'ITAD_KEY' => $pcgwSecrets["ITAD_KEY"]
        ]
    ],
//    'edgCacheTable' => [
//        'default' => 'ed_url_cache'
//    ],
//    'edgCacheExpireTime' => [
//        'default' => 1 * 24 * 60 * 60
//    ],
    */
    // Misc
    'wgDebugComments' => [
        'default' => false
    ],
    'wgRestrictDisplayTitle' => [
        'default' => false
    ],
    // Email
    'wgSMTP' => [
        'default' => [
            'host' => 'smtp.postmarkapp.com',
            'IDHost' => 'pcgamingwiki.com',
            'port' => 587,
            'auth' => true,
            'username' => $pcgwSecrets["SMTP_PASSWORD"],
            'password' => $pcgwSecrets["SMTP_PASSWORD"]
        ]
    ],
    // Debugging
    'wgShowSQLErrors' => [
        'default' => false
    ],
    'wgShowDBErrorBacktrace' => [
        'default' => false
    ],
    'wgShowExceptionDetails' => [
        'default' => true
    ],
    'wgDebugLogGroups' => [
        'default' => [
            'exception' => '/run/nginx/mw.log',
            'error' => '/run/nginx/mw.log'
        ]
    ],
    'wgStructuredChangeFiltersShowPreference' => [
        'default' => true
    ],
    'wgStructuredChangeFiltersShowWatchlistPreference' => [
        'default' => true
    ],
        // SSO
        'wgKeycloakAuthLoginUrl' => [
            'default' => 'https://auth.pcgamingwiki.com/oauth2/start?rd=$1',
            'applegamingwiki' => 'https://auth.applegamingwiki.com/oauth2/start?rd=$1',
        ],
        'wgKeycloakAuthLogoutUrl' => [
            'default' => 'https://auth.pcgamingwiki.com/oauth2/sign_out?rd=https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/protocol/openid-connect/logout?redirect_uri=$1',
            'applegamingwiki' => 'https://auth.applegamingwiki.com/oauth2/sign_out?rd=https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/protocol/openid-connect/logout?redirect_uri=$1'
        ],
        'wgKeycloakAuthPortalUrl' => [
            'default' => 'https://sso.pcgamingwiki.com/auth/realms/PCGamingWiki/account'
        ],
        'wgKeycloakAuthInsecureHeaders' => [
            'default' => true
        ]
];


if (defined('MW_DB')) {
    $wgDBname = MW_DB;
} else {
    $host = '';
    if (isset($_SERVER["HTTP_X_FORWARDED_HOST"])) {
        $host = $_SERVER["HTTP_X_FORWARDED_HOST"];
    } else {
        $host = $_SERVER["HTTP_HOST"];
    }
    switch ($host) {
        case 'www.pcgamingwiki.com':
        case 'images.pcgamingwiki.com':
        case 'thumbnails.pcgamingwiki.com':
            $wgDBname = 'pcgamingwiki';
            break;
/*
        case 'www.ftlwiki.com':
        case 'images.ftlwiki.com':
        case 'thumbnails.ftlwiki.com':
            //$wgDBname = 'ftlwiki';
            header('Location: http://www.pcgamingwiki.com/');
            break;
        case 'www.gunpointwiki.net':
        case 'images.gunpointwiki.net':
        case 'thumbnails.gunpointwiki.net':
            //$wgDBname = 'gunpointwiki';
            header('Location: http://www.pcgamingwiki.com/');
            break;
        case 'www.prisonarchitectwiki.com':
        case 'images.prisonarchitectwiki.com':
        case 'thumbnails.prisonarchitectwiki.com':
            //$wgDBname = 'prisonarchitectwiki';
            header('Location: http://www.pcgamingwiki.com/');
            break;
        case 'www.siryouarebeinghuntedwiki.com':
        case 'images.siryouarebeinghuntedwiki.com':
        case 'thumbnails.siryouarebeinghuntedwiki.com':
            //$wgDBname = 'siryouarebeinghuntedwiki';
            header('Location: http://www.pcgamingwiki.com/');
            break;
*/
        case 'www.applegamingwiki.com':
        case 'images.applegamingwiki.com':
        case 'thumbnails.applegamingwiki.com':
            $wgDBname = 'applegamingwiki';
            break;
        default:
            /*
            header('HTTP/1.1 404 Not Found');
            $host = $_SERVER['HTTP_HOST'];
            echo 'You shouldn\'t be seeing this. If this truly is an error, please notify an administrator.';
            echo '\n';
            echo "Couldn't map $host to a wiki";
            */
            header('Location: http://www.pcgamingwiki.com/');
            exit(0);
    }
}

$pcgwLoadExtensions = [];
$pcgwLoadSkins = [];
$wgConf->extractAllGlobals($wgDBname);

foreach ($pcgwLoadExtensions as &$extension) {
    switch ($extension) {
        case 'googleAnalytics':
            require_once "$IP/extensions/googleAnalytics/googleAnalytics.php";
            break;
        case 'HTMLets':
            require_once "$IP/extensions/HTMLets/HTMLets.php";
            break;
        case 'SubpageFun':
            require_once "$IP/extensions/SubpageFun/SubpageFun.php";
            break;
        default:
            wfLoadExtension($extension);
    }
}

foreach ($pcgwLoadSkins as &$skin) {
    switch ($skin) {
        case 'pcgwnvector':
            require_once "$IP/skins/pcgwnvector/pcgwnvector.php";
            break;
        case 'overclocked':
            require_once "$IP/skins/overclocked/Overclocked.php";
            break;
        case 'appleclocked':
            require_once "$IP/skins/appleclocked/Overclocked.php";
            break;
        default:
            wfLoadSkin($skin);
    }
}

if (isset($pcgwDefineNamespaces)) {
    config_configureNamespaces($pcgwDefineNamespaces);
}

/* Hopefully disables local login/logout and password reset special pages
 * From: https://www.mediawiki.org/wiki/Manual:Special_pages#Disabling_Special:UserLogin_and_Special:UserLogout_pages
 */
 /* Disabled for now since this might actually be useful to recover some old accounts, I guess?
$wgHooks['SpecialPage_initList'][] = function ( &$list ) {
	unset( $list['Userlogout'] );
	unset( $list['Userlogin'] );
	return true;
};
*/

/* Workaround for the googleAnalytics extension being dumb and initializing all of this config on load instead of in a
 * hook like it should.
 */
$wgConf->extractGlobal('wgGoogleAnalyticsAccount', $wgDBname);
